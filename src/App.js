import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Items from './components/Items'
import Result from './components/Result'
export default class App extends Component {
  constructor() {
    super();
    this.state = {
      items: [{
        id: 1,
        profile: 'https://i.insider.com/5c0990b1d5000c07f77ba114?width=960&format=jpeg',
        title: 'Pizza',
        price: 15,
        amount: 0,
        total: 0
      },
      {
        id: 2,
        profile: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimg1.cookinglight.timeinc.net%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2F1542062283%2Fchocolate-and-cream-layer-cake-1812-cover.jpg%3Fitok%3DrEWL7AIN',
        title: 'Burger',
        price: 5,
        amount: 0,
        total: 0
      }, {
        id: 3,
        profile: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimg1.cookinglight.timeinc.net%2Fsites%2Fdefault%2Ffiles%2Fstyles%2Fmedium_2x%2Fpublic%2F1542062283%2Fchocolate-and-cream-layer-cake-1812-cover.jpg%3Fitok%3DrEWL7AIN',
        title: 'Cake',
        price: 3,
        amount: 0,
        total: 0
      }],
    }
  }
  add = (id) => {
    let items = [...this.state.items]
    items[id].amount = items[id].amount + 1
    items[id].total = items[id].amount * items[id].price
    this.setState({
      items
    })
  }
  
  selectItems = () => {
    let items = this.state.items.filter((item) => item.amount > 0)
    return items
  }

  delete = (id) => {
    let items = [...this.state.items]
    items[id].amount = items[id].amount - 1
    items[id].total = items[id].amount * items[id].price
    this.setState({
      items
    })
  }
  render() {
    return (
      <div>
        <Items items={this.state.items} onAdd={this.add} onDelete={this.delete} />
        <Result items={this.selectItems()} />
      </div>
    )
  }
}

