import React, { Component } from 'react'
import { Card, Button } from 'react-bootstrap';
export default class Item extends Component {
    render() {
        return (
            <div className="col-md-4 block-card">
                <Card>
                    <Card.Img variant="top" src={this.props.item.profile} />
                    <Card.Body>
                        <Card.Title>{this.props.item.title}</Card.Title>
                        <Card.Text>
                        Price: {this.props.item.price}$ <br/>
                        Amount: {this.props.item.amount}    
                        </Card.Text>
                        Total: {this.props.item.total}$
                        <Button variant="primary" className="mx-2"  onClick={() => this.props.onAdd(this.props.item.id - 1)}>Add Card</Button>
                        <Button variant="danger" onClick={() => this.props.onDelete(this.props.item.id - 1)}  disabled={this.props.item.amount === 0 ? true: false}>Delete</Button>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}
