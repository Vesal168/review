import React from 'react'
import { Table } from 'react-bootstrap';

export default function Result(props) {
    let Items = props.items.map((item) =>
        <tr key={item.id}>
            <td>{item.id}</td>
            <td>{item.price}</td>
            <td>{item.amount}</td>
            <td>{item.total}</td>
        </tr>
    )
    return (
        <div className="container">
            <Table striped bordered hover className="m-top">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Price</th>
                        <th>Amount</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                  {Items}
                </tbody>
            </Table>
        </div>
    )
}
